0 conf{/*{{{*/
    0.1 基于域名的虚拟机{/*{{{*/
        Include conf/extra/httpd-vhosts.conf  #打开include vhost
        
        vim /opt/apache/conf/extra/httpd-vhosts.conf 
        <VirtualHost *:80>
            ServerAdmin dengrongfei@ztgt.cn
            DocumentRoot "/global/crz/"
            ServerName crz.cn
            ErrorLog "logs/crz-error.log"
            CustomLog "logs/crz-access.log" common
        </VirtualHost>
        
        vim /etc/hosts
        127.0.0.1 crz.cn
    
    }/*}}}*/


}/*}}}*/

