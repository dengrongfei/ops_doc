0 svn{


}


1 git{
    git文档  http://git.oschina.net/progit/index.html
    1.1 配置{
        .gitignore #排除同步文件
            cat .gitignore
            *.[oa] 排除.o 或 .a
            *~     忽略所有以波浪符～结尾的文件
            # 井号开始的行被忽略
            *.a      # 忽略所有 .a 结尾的文件
            !lib.a   # 但 lib.a 除外
            /TODO    # 仅仅忽略项目根目录下的 TODO 文件，不包括 subdir/TODO
            build/   # 忽略 build/ 目录下的所有文件
            doc/*.txt   # 会忽略 doc/notes.txt 但不包括 doc/server/arch.txt

    }

    1.2 命令{
       分支{
            git branch -v       --查看分支带前面带*的是当前分支
            git checkout 分支名 --切换分支
   }
   
        tag{ 
            git tag             --查看tag
            git tag -l v1.4.2.* --过滤标签（只查看v1.4.2.M 的版本)
            git tag v0.1        --轻量级打包tag
            git tag -a v0.1 -m 'version 0.1'  --带注释信息的打包(推荐)
            git show v0.1       --  查看某个版本的具体信息
            git push origin  –tags      --同步所有标签（还没验证过)
            git push origin v0.1        --同步某一版本


        }
       


        }



}
