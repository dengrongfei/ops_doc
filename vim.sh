0 基本操作{#{{{
    ctrl + [   等于esc


}    #}}}
1 折叠设置{#{{{
    in .vimrc file set default foldmethod
    set foldmethod=indent 
    
    
    1.1 折叠方式{#{{{
        可用选项 'foldmethod' 来设定折叠方式：set fdm=*****。
        有 6 种方法来选定折叠：
        manual           手工定义折叠
        indent           更多的缩进表示更高级别的折叠
        expr             用表达式来定义折叠
        syntax           用语法高亮来定义折叠
        diff             对没有更改的文本进行折叠
        marker          对文中的标志折叠
        注意，每一种折叠方式不兼容，如不能即用expr又用marker方式，我主要轮流使用indent和marker方式进行折叠。
    }#}}}

    1.2 折叠命令{#{{{
        选取了折叠方式后，我们就可以对某些代码实施我们需要的折叠了，由于我使用indent和marker稍微多一些，故以它们的使用为例：
        如果使用了indent方式，vim会自动的对大括号的中间部分进行折叠，我们可以直接使用这些现成的折叠成果。
        在可折叠处（大括号中间）：
    


        zfap -- 折叠光标所在的段；
        zo -- 打开折叠的文本；
        zc -- 收起折叠；
        za -- 打开/关闭当前折叠
        zr -- 打开嵌套的折行
        zm -- 收起嵌套的折行
        zR (zO) -- 打开所有折行
        zM (zC) -- 收起所有折行
        zj -- 跳到下一个折叠处
        zk -- 跳到上一个折叠处
        zi -- enable/disable fold
        zd -- 删除当前行的折叠；
        zD -- 删除当前行的折叠；

        当使用marker方式时，需要用标计来标识代码的折叠，系统默认是{{{和}}}，最好不要改动之：）
        我们可以使用下面的命令来创建和删除折叠：

        zf -- 创建折叠的命令，可以在一个可视区域上使用该命令 #manual/marker method
        zf56G -- 创建从当前行起到56行的代码折叠；
        zf% --   创建当前行到%匹配到的括号
        10zf  -- 或10zf+或zf10↓，创建从当前行起到后10行的代码折叠。
        10zf-或zf10↑ -- 创建从当前行起到之前10行的代码折叠。
        在括号处zf%  -- 创建从当前行起到对应的匹配的括号上去（（），{}，[]，<>等）。
        zd    --   删除 (delete) 在光标下的折叠。仅当 'foldmethod' 设为 "manual" 或 "marker" 时有效。
        zD    --  循环删除 (Delete) 光标下的折叠，即嵌套删除折叠。
        仅当 'foldmethod' 设为 "manual" 或 "marker" 时有效。
        zE     除去 (Eliminate) 窗口里“所有”的折叠。
        仅当 'foldmethod' 设为 "manual" 或 "marker" 时有效
    }#}}}


} #}}}

2 命令行模式{#{{{
    2.0 命令行基础{#{{{
         :version
    
    
    
    
    }#}}}

    2.1 执行外部命令{#{{{
        :!cmd   执行外部命令
        :r!cmd  执行外部命令，结果插入到光标位置 
        :sh     调用shell，exit返回vim
        :m,nw !cmd 将文件的m行到n行之间的内容做为命令输入执行命令or参数，返回结果替换当前内容
    }#}}}

    2.2 多标签{#{{{
        进入vim前
        vim -p <文件名> 以多标签形式打开文件。如vim -p * 就是编辑当前目录的所有文件，
        vim编辑中
        :tabnew     增加一个标签
        :tabc       关闭当前的tab
        :tabo       关闭所有其他的tab
        :tabs       查看所有打开的tab
        :tabp 或gT  前一个
        :tabn 或gt  后一个

        新建标签页
        :tabe <文件名>  在新标签页中打开指定的文件。
        :tabnew <文件名>  在新标签页中编辑新的文件。
        :tab split  在新标签页中，打开当前缓冲区中的文件。
        :tabf  允许你在当前目录搜索文件，并在新标签页中打开。比如:tabf img.*。此命令只能打开一个文件.

        Vim默认最多只能打开10个标签页。你可以用set tabpagemax=15改变这个限制。

        列示标签页
        :tabs  显示已打开标签页的列表，并用“>”标识出当前页面，用“+”标识出已更改的页面。

        关闭标签页
        :tabc  关闭当前标签页。
        :tabo  关闭所有的标签页。

        切换标签
        :tabn或gt  移动到下一个标签页。
        :tabp或gT  移动到上一个标签页。
        :tabfirst或:tabr  移动到第一个标签页。
        :tablast  移动到最后一个标签页。

        移动标签页

        :tabm [次序]  移动当前文件的标签页次序。比如:tabm 1将把当前标签页移动到第2的位置。如:tabm不指定参数将被移动到最后。

        配置标签页
        :set showtabline=[1,2,3]  标签页在窗口上方显示标签栏。=0完全不显示标签栏，=1只有用户新建时才显示，=2总是显示标签栏。

        多标签页命令
        :tabdo <命令>  同时在多个标签页中执行命令。比如:tabdo %s/food/drink/g 命令把当前多个标签页文件中的“food”都替换成“drink”。

        帮助

        :help tab-page-intro命令，可以获得关于标签页使用的更多信息。 
            
      }#}}}


    2.3 多窗口{#{{{
        1、打开多个窗口（vim编辑中）
            打开多个窗口的命令以下几个：
            横向切割窗口
            :new+窗口名(保存后就是文件名) 
            :split+窗口名，也可以简写为:sp+窗口名
            纵向切割窗口名
            :vsplit+窗口名，也可以简写为：vsp+窗口名
             
                vim -o file1 file2 ...  #水平打开多窗口，
                vim -O file1 file2 ...  #垂直打开多窗口,
                vim -d file1 file2 ...  #垂直打开多窗口,并且进行比较
                vimdiff file1 file2 ..  #等同于上一句

        2、关闭多窗口
            可以用：q!，
            也可以使用：close，最后一个窗口不能使用close关闭。
            使用close只是暂时关闭窗口，其内容还在缓存中，只有使用q!、w!或x才能真能退出。
             关闭窗口
             q  或 close   #关闭当前窗口
             only          #保留当前窗口，关闭其它所有窗口
             qall(qa)          #退出所有窗口
             wall          #保存所有窗口


        3、窗口切换
        :ctrl+w+j/k，通过j/k可以上下切换，或者:ctrl+w加上下左右键，还可以通过快速双击ctrl+w依次切换窗口。

        4、窗口大小调整
            纵向调整
            :ctrl+w + 纵向扩大（行数增加）
            :ctrl+w - 纵向缩小 （行数减少）
            :res(ize) num  例如：:res 5，显示行数调整为5行
            :res(ize)+num 把当前窗口高度增加num行
            :res(ize)-num 把当前窗口高度减少num行
            横向调整
            :vertical res(ize) num 指定当前窗口为num列
            :vertical res(ize)+num 把当前窗口增加num列
            :vertical res(ize)-num 把当前窗口减少num列

        5、给窗口重命名
        :f file 
        }#}}}
}#}}}

3 文件浏览{#{{{
    :Ex 开启目录浏览器，可以浏览当前目录下的所有文件，并可以选择
    :Sex 水平分割当前窗口，并在一个窗口中开启目录浏览器
    :ls 显示当前buffer情况
}#}}}

4 书签{#{{{
    m{a-zA-Z} -> 保存书签，小写的是文件书签，
         可以用(a-z）中的任何字母标记。大写的是全局 书签，
         用大写的(A-Z)中任意字母标记。(mark position as bookmark. when lower, only stay in file. when upper, stay in global)
    `{a-zA-Z} -> 跳转到某个书签。
        如果是全局书签，则会开启被书签标记的文件跳转至标记的行 
    `0 -> 跳转入现在编辑的文件中上次退出的位置 (go to last exit in file)
    " -> 跳转如最后一次跳转的位置 (go to last jump -> go back to last jump)
    '" -> 跳转至最后一次编辑的位置 (go to last edit)
    g`{mark} -> 跳转到书签 (jump to {mark})
    :delm{marks} -> 删除一个书签 (delete a mark) 例如:delma那么就删除了书签a
    :delm! -> 删除全部书签 (delete all marks)
    :marks -> 显示系统全部书签 (show all bookmarks)

}#}}}

5 光标移动{#{{{

    5.1 基本移动{#{{{
    以下移动都是在normal模式下。

        h或退格: 左移一个字符；
        l或空格: 右移一个字符；
        j: 下移一行；
        k: 上移一行；
        gj: 移动到一段内的下一行；
        gk: 移动到一段内的上一行；
        +或Enter: 把光标移至下一行第一个非空白字符。
        -: 把光标移至上一行第一个非空白字符。
        w: 前移一个单词，光标停在下一个单词开头；
        W: 移动下一个单词开头，但忽略一些标点；
        e: 前移一个单词，光标停在下一个单词末尾；
        E: 移动到下一个单词末尾，如果词尾有标点，则移动到标点；
        b: 后移一个单词，光标停在上一个单词开头；
        B: 移动到上一个单词开头，忽略一些标点；
        ge: 后移一个单词，光标停在上一个单词末尾；
        gE: 同 ge ，不过‘单词’包含单词相邻的标点。
        (: 前移1句。
        ): 后移1句。
        {: 前移1段。
        }: 后移1段。
        fc: 把光标移到同一行的下一个c字符处
        Fc: 把光标移到同一行的上一个c字符处
        tc: 把光标移到同一行的下一个c字符前
        Tc: 把光标移到同一行的上一个c字符后
        ;: 配合f & t使用，重复一次
        ,: 配合f & t使用，反向重复一次

    上面的操作都可以配合n使用，比如在正常模式(下面会讲到)下输入3h， 则光标向左移动3个字符。

        0: 移动到行首。
        g0: 移到光标所在屏幕行行首。
        ^: 移动到本行第一个非空白字符。
        g^: 同 ^ ，但是移动到当前屏幕行第一个非空字符处。
        $: 移动到行尾。
        g$: 移动光标所在屏幕行行尾。
        n|: 把光标移到递n列上。
        nG: 到文件第n行。
        :n<cr> 移动到第n行。
        :$<cr> 移动到最后一行。
        H: 把光标移到屏幕最顶端一行。
        M: 把光标移到屏幕中间一行。
        L: 把光标移到屏幕最底端一行。
        gg: 到文件头部。
        G: 到文件尾部。
    }#}}}

    5.2 翻屏{#{{{
        ctrl+f: 下翻一屏。
        ctrl+b: 上翻一屏。
        ctrl+d: 下翻半屏。
        ctrl+u: 上翻半屏。
        ctrl+e: 向下滚动一行。
        ctrl+y: 向上滚动一行。
        n%: 到文件n%的位置。
        zz: 将当前行移动到屏幕中央。
        zt: 将当前行移动到屏幕顶端。
        zb: 将当前行移动到屏幕底端。
    }#}}}

    5.3 标记

    使用标记可以快速移动。到达标记后，可以用Ctrl+o返回原来的位置。 Ctrl+o和Ctrl+i 很像浏览器上的 后退 和 前进 。

        m{a-z}: 标记光标所在位置，局部标记，只用于当前文件。
        m{A-Z}: 标记光标所在位置，全局标记。标记之后，退出Vim， 重新启动，标记仍然有效。
        `{a-z}: 移动到标记位置。
        `{a-z}: 移动到标记行的行首。
        `{0-9}：回到上[2-10]次关闭vim时最后离开的位置。
        ``: 移动到上次编辑的位置。''也可以，不过``精确到列，而''精确到行 。如果想跳转到更老的位置，可以按C-o，跳转到更新的位置用C-i。
        `": 移动到上次离开的地方。"
        `.: 移动到最后改动的地方。
        :marks 显示所有标记。
        :delmarks a b -- 删除标记a和b。
        :delmarks a-c -- 删除标记a、b和c。
        :delmarks a c-f -- 删除标记a、c、d、e、f。
        :delmarks! -- 删除当前缓冲区的所有标记。
        :help mark-motions 查看更多关于mark的知识。

 

}#}}}


6 打开多文件{#{{{
    vi打开多文件（进入vim前）
    vi a b c
    :n 跳至下一个文件，也可以直接指定要跳的文件，如:n c，可以直接跳到c文件
    :e# 回到刚才编辑的文件
    }#}}}


7 插件管理{#{{{
    bundle{#{{{
        安装&配置文件
        1. git clone https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle
        2. vim ~/.vimrc
           source ~/.vim/bundles.vim
        3. vim ~/.vim/bundles.vim
          set nocompatible
          filetype off 
          set rtp+=~/.vim/bundle/Vundle.vim
          call vundle#begin()
          let Vundle mange vundle,required
          Plugin 'gmarik/Vundle.vim'
          ""Plugin 'tpope/vim-fugitive'
          Plugin 'kien/ctrlp.vim'
          Plugin 'tomasr/molokai'
          #~/.vim/bundle/Vundle.vim  是目录需要创建
          #三种插件导入方式,上面是导入github里面的插件
          

          命令：
          BundleInstall 安装
          BundleUpdate  更新
          Bundlesearch  搜索
          BundleClean   卸载
          BundleDoc     文档
          BundleList    已经安装的列表

          }#}}}




}#}}}
